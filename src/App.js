import React from "react";
import {questions} from "./data/questions-data";
import Quiz from "./components/Quiz";
import "./App.css";
import "./bootstrap.min.css";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <Quiz questions={questions} />
        </div>
      </div>
    </div>
  );
}

export default App;
