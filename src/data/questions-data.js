export const questions = [
    {id: 1, title: "What is the capital of Fiji?", options:["Suva", "Nadi", "Levuka", "Ba"], answer:"Suva"},
    {id: 2, title: "What does not fly?", options:["Helicopter", "Airplane", "Car", "Fighter Jet"], answer:"Car"},
    {id: 3, title: "What is the colour of grass?", options:["Yellow", "Red", "Blue", "Green"], answer:"Green"},
    {id: 4, title: "Which direction does the sun rise from?", options:["North", "South", "East", "West"], answer:"East"},
    {id: 3, title: "What is the colour of the sky on a sunny day?", options:["Yellow", "Red", "Blue", "Green"], answer:"Blue"},
]