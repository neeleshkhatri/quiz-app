import React from "react";
import "./Card.css";

function Card({ header, body, footer }) {
  return (
    <div className="card">
      <div className="card-header">{header()}</div>
      <div className="card-body minheight">{body()}</div>
      <div className="card-footer fixed-height">{footer()}</div>
    </div>
  );
}

export default Card;
