import React, { Component } from "react";
import Welcome from "./Welcome";
import Question from "./Question";
import Card from "./Card";

const quizState = {
  WELCOME: "welcome",
  QUESTION: "question",
  COMPLETE: "complete",
};

export default class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quizState: quizState.WELCOME,
      index: null,
      question: null,
      isCorrectAnswer: null,
      score: 0,
      submitButton: { text: "Start", disabled: false },
    };
  }

  header = () => {
    return (
      <>
        <span>Quiz</span>
        <span className="float-right">Score: {this.state.score}</span>
      </>
    );
  };

  body = () => {
    switch (this.state.quizState) {
      case quizState.WELCOME:
        return (
          <Welcome
            question={this.state.question}
            numberOfQuestions={this.props.questions.length}
            nextQuestion={this.nextQuestion}
          />
        );
      case quizState.QUESTION:
        return (
          <>
            <Question
              question={this.state.question}
              checkAnswer={this.checkAnswer}
            />
          </>
        );
      case quizState.COMPLETE:
        return (
          <div class="row">
          <div class="col-lg-12 text-center"><h3>You have finished the quiz.</h3></div>
          <div class="col-lg-12 text-center"><h3>Your total score is {this.state.score} out of {this.props.questions.length}.</h3></div>
          </div>
          );
      default:
        return null;
    }
  };

  footer = () => {
    if (this.state.quizState === quizState.COMPLETE) return null;
    return (
      <button
        type="submit"
        className="btn btn-primary float-right"
        disabled={this.state.submitButton.disabled}
      >
        {this.state.submitButton.text}
      </button>
    );
  };

  render() {
    return (
      <form onSubmit={this.nextQuestion}>
        <Card header={this.header} body={this.body} footer={this.footer} />
      </form>
    );
  }

  checkAnswer = (event) => {
    const answer = event.target.value;
    const question = this.state.question;

    if (question) {
      const isCorrect = question.answer === answer;
      this.setState({
        ...this.state,
        submitButton: { text: "Next", disabled: false },
        isCorrectAnswer: isCorrect,
      });
    }
  };

  nextQuestion = (event) => {
    event.preventDefault();
    const isLastQuestion = this.state.index === this.props.questions.length - 1;
    let index = this.state.index || 0;
    if (this.state.question && !isLastQuestion) {
      index = this.state.index + 1;
    }

    this.setState({
      ...this.state,
      quizState: !isLastQuestion ? quizState.QUESTION : quizState.COMPLETE,
      index: index,
      question: this.props.questions[index],
      submitButton: { text: "Next", disabled: true },
      score: this.state.isCorrectAnswer
        ? this.state.score + 1
        : this.state.score,
    });
  };
}
