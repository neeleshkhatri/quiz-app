import React from "react";

function Question({ question, checkAnswer }) {
  return (
    <>
      <div>
        <h4>Question {question.id}: {question.title}</h4>
      </div>
      <div>
        {question.options.map((o) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="answer"
              key={o}
              id={o}
              value={o}
              onChange={checkAnswer}
            />
            <label className="form-check-label" htmlFor={question.id}>
              {o}
            </label>
          </div>
        ))}
      </div>
    </>
  );
}

export default Question;
