import React from "react";

function Welcome({ question, numberOfQuestions }) {
  return (
    <>
      <h3 className="card-title">Welcome to the quiz.</h3>
      <p className="card-text">There are {numberOfQuestions} questions.</p>
      <p className="card-text">Please click start to being the quiz.</p>
    </>
  );
}

export default Welcome;
